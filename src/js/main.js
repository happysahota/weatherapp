import React from "react";
import ReactDom from "react-dom";

import Weatherman from "./components/Weatherman";
import "bulma";
import "../scss/style.scss";

var app = document.getElementById("app");
ReactDom.render(<Weatherman/>, app);