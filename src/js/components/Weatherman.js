import React from "react";
import axios from "axios";
import { setTimeout, setInterval } from "timers";

export default class Weatherman extends React.Component {

    dialyUpperLowerTemp = [];

    constructor() {
        super();
        this.state = {
            city: 'Delhi',
            weatherData: []
        };

        // to fetch the initial temprature values
        this.getTemprature();
        //this is to set scope into citySetter function
        this.citySetter = this.citySetter.bind(this)
    }

    getTemprature(city) {

        var selectedCity = city? city:this.state.city;
        var callBackThis = this;

        axios.get("https://api.openweathermap.org/data/2.5/forecast?q=" + selectedCity + ",IN&appid=726faf11f86f63988b94479c15e877c5&units=metric")
            .then(function (data) {
                callBackThis.changeWeatherData(data.data, selectedCity);
            });
    }

    changeWeatherData(data, selectedCity) {
        data = this.weatherFilter(data);
        this.setState({
                        'city': selectedCity,
                        'weatherData': data
                    });
    }

    // higher and min temp filter for the day
    weatherFilter(data) {
        var list = data.list
        var ret = {};
        list.map(data => {

            //making unique date format
            var dt = new Date(data.dt_txt);
            var dtFormat = [
                ('0' + dt.getDate()).slice(-2),
                ('0' + (dt.getMonth() + 1)).slice(-2),
                dt.getFullYear(),
            ].join('-');

            // creatting unique date vise data
            if (Object.keys(ret).indexOf(dtFormat) == -1) {
                ret[dtFormat] = { 'min': data.main.temp, 'max': data.main.temp };
            }

            //finding temp Max / Min values
            if (data.main.temp < ret[dtFormat].min) {
                ret[dtFormat].min = data.main.temp;
            }
            if (data.main.temp > ret[dtFormat].max) {
                ret[dtFormat].max = data.main.temp;
            }

        });

        return ret;
    }

    citySetter(event) {
        var selectedCity = event.target.value;
        // setTimeout(() => {
        //     this.setState(() => {
        //         return { 'city': selectedCity }
        //     });
        // }, 0)
        this.getTemprature(selectedCity);        
    }

    render() {

        var tempList = this.state.weatherData;
        var tempData = [];

        
         console.log(tempList);

        for(var key in tempList) {
            tempData.push(<tr key={key}>
                <th>{key}</th>
                <td>{tempList[key].max} &#8451;</td>
                <td>{tempList[key].min} &#8451;</td>
            </tr>);
        }

        return (
            <div class="column is-three-fifths alignCenter">

                <table class="table alignCenter is-bordered is-striped is-narrow is-hoverable ">
                    <thead>
                        <tr>
                            <th colSpan="5" class="has-text-centered weatherTitle">
                                Select your city: <select onChange={this.citySetter} value={this.state.city}>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Pune">Pune, Maharashtra</option>
                                    <option value="Shimla">Shimla, Himachal</option>
                                    <option value="chennai">Chennai, Tamilnadu</option>
                                    <option value="Bathinda">Bathinda, Punjab</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th colSpan="5" class="has-text-centered weatherTitle">5 days Weather forcast for {this.state.city}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Date(dd/mm/yyyy)</th>
                            <th>Max</th>
                            <th>Min</th>
                        </tr>
                        {tempData}
                    </tbody>
                </table>

            </div>
        );
    }
}