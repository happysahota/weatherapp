### Decsriptive Doc

This is demo where we have used http://openweathermap.org/forecast5 library to give 5 day forcast of temprature. Initially it will load weather forcast for only Delhi but user can check his/her prefered city out of 5 provided cities. 

#### Technologies and tools used

1. ReactJS => I've used ReactJS fo rthis demo as this will demonstrate my knowledge of both React and ES6.

2. Axios => lite weight Asynchronous ajax requesting library as such helper doesn't came out of the box from React.

3. Bulma => its also a lite weight css.

4. SASS => As we have used Bulma which can be used from CDN I have used NPM version so that to demonstrate the possibility to extend Bulma as per own requirement in very helpful way using SCSS.


### Steps to setup on local.

1. Clone the GIT repository.

2. Chane directory to the root of the cloned repository.

2. Execute command.. 
    npm install (Windows)
    sudo npm install (Linux)

3. After all the packages get installed, execute any of following command:
    1. To see site loaded localy for demo: npm run dev
    2. For minified deployable code: npm run build:prod
    3. For unminified Deployable code: npm run build

    After "npm run dev" you can check site on URL http://localhost:8283

4. For testing the code just execute:
    npm test

### Future develoopment with more time

1. By investing little more time we can convert it to more interactive and better ui.
2. We can used Reduc / Flux for better data flow.
3. We can implement DB so that we can maintain historic data for any user requirement.
4. We can introduce user login functionality so that users can maintain profile.
5. Mobile app can be made of similar fashion.


### Demo website hosted at: https://happysahota007.000webhostapp.com/ 
 This domain  sleeps for 1 hour after every 10 hours so if you found it not working please wait and retry.... My bad this is free host. :P

 ### if demo site give 502 error just do hard refresh... CTRL+F5
