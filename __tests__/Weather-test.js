import React from 'react';
import { configure, shallow } from 'enzyme';

import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import Weatherman from '../src/js/components/Weatherman';

test('Title Check', () => {
    const wrapper = shallow(<Weatherman />);
    expect(wrapper.find('table thead tr th').text()).toEqual('5 days Weather forcast for Delhi');
});


test('Record Count Check', () => {
    const wrapper = shallow(<Weatherman />);
    expect(wrapper.find('table tbody tr').length).toBe(5);
});